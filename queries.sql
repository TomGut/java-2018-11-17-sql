--PREPARATION AS ADMIN

sudo mysql
CREATE USER 'homework' IDENTIFIED BY 'homework';
CREATE DATABASE homework CHARACTER SET utf8 COLLATE utf8_general_ci;
GRANT ALL ON homework.* TO 'homework';

--MAIN CODE FOR HOMEWORK BELOW

mysql -u homework -h localhost -p
--password: 'homework'
USE homework;

--SCRIPT START

CREATE TABLE dragons (
name VARCHAR (256) PRIMARY KEY,
color VARCHAR (256),
wingspan INT
);

CREATE TABLE eggs (
weight INT,
diameter INT
);

ALTER TABLE eggs ADD COLUMN dragon VARCHAR (256) NOT NULL;
ALTER TABLE eggs ADD FOREIGN KEY (dragon) REFERENCES dragons (name);

CREATE TABLE decoration (
color VARCHAR (256),
pattern VARCHAR (256)
);

ALTER TABLE eggs ADD COLUMN id INT NOT NULL AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE decoration ADD COLUMN eggID INT NOT NULL PRIMARY KEY;
ALTER TABLE decoration ADD FOREIGN KEY (eggID) REFERENCES eggs (id);

CREATE TABLE lands (
name VARCHAR (256)
);

CREATE TABLE landsAndDragons (
dragonName VARCHAR (256),
landName VARCHAR (256),
PRIMARY KEY (dragonName, landName)
);

ALTER TABLE landsAndDragons ADD FOREIGN KEY (dragonName) REFERENCES dragons(name);

INSERT INTO dragons (name, color, wingspan) VALUES 
('Dygir', 'green', 200), ('Bondril', 'red', 100), 
('Onosse', 'black', 250), ('Chiri', 'yellow', 50), 
('Lase', 'blue', 300);

INSERT INTO eggs (weight, diameter, dragon) VALUES 
(300, 20, (SELECT name FROM dragons WHERE wingspan=200)), 
(340, 30, (SELECT name FROM dragons WHERE wingspan=200)), 
(200, 10, (SELECT name FROM dragons WHERE wingspan=100)), 
(230, 20, (SELECT name FROM dragons WHERE wingspan=250)), 
(300, 25, (SELECT name FROM dragons WHERE wingspan=250)), 
(200, 15, (SELECT name FROM dragons WHERE wingspan=250));

INSERT INTO decoration (color, pattern, eggID) VALUES 
('pink', 'mesh', (SELECT id FROM eggs WHERE weight=300 AND diameter=20)), 
('black', 'striped', (SELECT id FROM eggs WHERE weight=340 AND diameter=30)), 
('yellow', 'dotted', (SELECT id FROM eggs WHERE weight=200 AND diameter=10));

INSERT INTO lands (name) VALUES 
('Froze'), ('Oswia'), ('Oscyae'), ('Oclurg');

INSERT INTO landsAndDragons (dragonName, landName) VALUES 
((SELECT name FROM dragons WHERE wingspan=200), 'Froze'), 
((SELECT name FROM dragons WHERE wingspan=100), 'Froze'), 
((SELECT name FROM dragons WHERE wingspan=200), 'Oswia'), 
((SELECT name FROM dragons WHERE wingspan=250), 'Oswia'), 
((SELECT name FROM dragons WHERE wingspan=50), 'Oswia');

CREATE VIEW eggs_info AS SELECT id, diameter, weight, decoration.color, decoration.pattern FROM eggs INNER JOIN decoration ON eggID = eggs.id;
CREATE VIEW dragons_lands AS SELECT dragonName, landName FROM landsAndDragons;
CREATE VIEW dragons_eggs AS SELECT dragons.name, weight, diameter FROM eggs INNER JOIN dragons ON dragon = dragons.name;
CREATE VIEW wingspan AS SELECT name FROM dragons WHERE wingspan>2 AND wingspan<4;

SELECT * FROM eggs_info;
SELECT * FROM dragons_lands;
SELECT * FROM dragons_eggs;
SELECT * FROM wingspan;
SELECT * FROM dragons;
SELECT * FROM eggs;
SELECT * FROM decoration;
SELECT * FROM lands;

DROP TABLE lands;
DROP TABLE decoration;
DROP TABLE eggs;
DROP TABLE landsAndDragons;
DROP TABLE dragons;
DROP VIEW eggs_info;
DROP VIEW dragons_lands;
DROP VIEW dragons_eggs;
DROP VIEW wingspan;